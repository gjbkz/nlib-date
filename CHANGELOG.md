# Changelog

## v0.1.5 (2021-10-04)

### Bug Fixes

- package settings ([e975134](https://github.com/nlibjs/date/commit/e975134d9534f20079d951ededdb74013198c0ae))


## v0.1.4 (2021-10-04)

### Features

- treat the input as a template string when it is a single string ([826da1c](https://github.com/nlibjs/date/commit/826da1c48cd843ef3498acb05fccaff3f9915817))

### Documentation

- update README ([9522fc7](https://github.com/nlibjs/date/commit/9522fc77a98ed0cad2326c6c1545b420e5cc440e))

### Dependency Upgrades

- @typescript-eslint/eslint-plugin:4.31.2→4.32.0 @typescript-eslint/parser:4.31.1→4.32.0 lint-staged:11.1.2→11.2.0 ([51235b2](https://github.com/nlibjs/date/commit/51235b26e1e6025623662d9eaa87338b79c38ff1))


## v0.1.3 (2021-09-06)

### Features

- freeze fragments ([9c58e04](https://github.com/nlibjs/date/commit/9c58e0424566bad7d54149e93e1b1c886673185b))

### Tests

- use @nlib/test@4.0.0 ([c6df781](https://github.com/nlibjs/date/commit/c6df781cb63db6293024a219ffc3831bbd84f0af))

### Code Refactoring

- use generator (#106) ([e48c057](https://github.com/nlibjs/date/commit/e48c057912666f91e1bf4fb44c9931039dd8dccd))

### Build System

- fix command ([7194db1](https://github.com/nlibjs/date/commit/7194db1d109172e22c81931a59eba2db626dc34d))

### Continuous Integration

- update workflow ([be89575](https://github.com/nlibjs/date/commit/be89575735e5ba28098facc8ea5a87dae41de6fb))

### Dependency Upgrades

- @nlib/eslint-config:3.17.24→3.17.25 @nlib/githooks:0.0.5→0.1.0 @nlib/test:3.18.7→4.0.0 ts-node:9.1.1→10.2.1 ([7330da2](https://github.com/nlibjs/date/commit/7330da2831a351145ad53e895050c9cc549c4380))
- uninstall @nlib/changelog @nlib/lint-commit ([ca7d844](https://github.com/nlibjs/date/commit/ca7d844508acfac295454ae620a38ac557c7ff11))
- @nlib/eslint-config:3.17.16→3.17.22 eslint:7.25.0→7.26.0 lint-s… (#105) ([001fc77](https://github.com/nlibjs/date/commit/001fc77fd9bf64df20765fe3e085341f0c4f1024))


## v0.1.2 (2020-10-03)

### Dependency Upgrades

- setup nlib-lint-commit ([2a33352](https://github.com/nlibjs/date/commit/2a33352222c80362be67862e9b2587308669cb6d))


## v0.1.1 (2020-09-07)

### Code Refactoring

- remove @nlib/typing ([95bf354](https://github.com/nlibjs/date/commit/95bf354827bb6181f4ab7a921428383027156c18))


## v0.1.0 (2020-09-07)

### Features

- add formatter for UTC ([8ffc88e](https://github.com/nlibjs/date/commit/8ffc88e5ddf9bd8d5cf7450f542c05c581e8fe12))

### Bug Fixes

- set TZ ([919ef05](https://github.com/nlibjs/date/commit/919ef057c081e7bb53a7dcef7ad71c60123574ec))

### Tests

- timezone ([7dc4d99](https://github.com/nlibjs/date/commit/7dc4d994950df78152f88aa6158acce14b1093c9))
- set TZ in the package.json ([9a5f6f0](https://github.com/nlibjs/date/commit/9a5f6f07ffbffe6625c3387b9812cb76373db2e0))

### Build System

- generate cjs and esm ([707c039](https://github.com/nlibjs/date/commit/707c039fa3fbfaee80f2792a79e4b2c00837d4d5))

### Continuous Integration

- update configurations ([c83dd84](https://github.com/nlibjs/date/commit/c83dd849dcf825e7771fd1d797fc59ba6bf5d566))

### Dependency Upgrades

- upgrade dependencies ([19fe461](https://github.com/nlibjs/date/commit/19fe461f212cc2553456e5aed5bead2ed501e3dc))


## v0.0.3 (2020-09-01)

### Bug Fixes

- upgrade typing ([1be4ae7](https://github.com/nlibjs/date/commit/1be4ae74dac41768e86800814bc5e67e66af6737))


## v0.0.2 (2020-09-01)

### Bug Fixes

- set TZ ([ca2254d](https://github.com/nlibjs/date/commit/ca2254d11e735bff365ca5908d0a722a4ffb6224))


## v0.0.1 (2020-09-01)

### Features

- add some formatters ([8f43fe7](https://github.com/nlibjs/date/commit/8f43fe7a5d2afca7cbd389e419e3ac0a054129b0))


