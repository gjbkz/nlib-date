import {testFunction} from '@nlib/test';
import {lISO8601DATE, uISO8601DATE, lISO8601TIME, lISO8601, uISO8601TIME, uISO8601} from './formatter';

const Date2020 = '2020-12-31T23:45:01.023Z';

testFunction(lISO8601DATE, [new Date(Date2020)], '2021-01-01');
testFunction(uISO8601DATE, [new Date(Date2020)], '2020-12-31');
testFunction(lISO8601TIME, [new Date(Date2020)], '08:45:01.023');
testFunction(lISO8601, [new Date(Date2020)], '2021-01-01T08:45:01.023+0900');
testFunction(uISO8601TIME, [new Date(Date2020)], '23:45:01.023');
testFunction(uISO8601, [new Date(Date2020)], '2020-12-31T23:45:01.023Z');
