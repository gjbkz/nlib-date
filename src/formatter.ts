import {lDD, lhh, lMM, lmm, lms, lss, lYYYY, lZZZ, uDD, uhh, uMM, umm, ums, uss, uYYYY} from './baseFormatter';
import {createDateFormatter} from './createDateFormatter';

export const lISO8601DATE = createDateFormatter(lYYYY, '-', lMM, '-', lDD);
export const uISO8601DATE = createDateFormatter(uYYYY, '-', uMM, '-', uDD);
export const lISO8601TIME = createDateFormatter(lhh, ':', lmm, ':', lss, '.', lms);
export const uISO8601TIME = createDateFormatter(uhh, ':', umm, ':', uss, '.', ums);
export const lISO8601 = createDateFormatter(lISO8601DATE, 'T', lISO8601TIME, lZZZ);
export const uISO8601 = createDateFormatter(uISO8601DATE, 'T', uISO8601TIME, 'Z');
