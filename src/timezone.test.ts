import {testFunction} from '@nlib/test';
import {parseTimeZoneOffset, getTimeZoneOffset} from './timezone';

testFunction(parseTimeZoneOffset, [-754], {sign: '+', hour: 12, minute: 34});
testFunction(parseTimeZoneOffset, [754], {sign: '-', hour: 12, minute: 34});
testFunction(getTimeZoneOffset, [new Date()], {sign: '+', hour: 9, minute: 0});
