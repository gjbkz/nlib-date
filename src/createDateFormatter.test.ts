import ava from 'ava';
import {udddd, uMMMM, uYYYY} from './baseFormatter';
import {createDateFormatter} from './createDateFormatter';

ava('empty string', (t) => {
    const template = '';
    const formatter = createDateFormatter(template);
    const date = new Date('2020-01-23T01:23:45.678Z');
    t.is(formatter(date), '');
});

ava('string without keywords', (t) => {
    const template = 'foo';
    const formatter = createDateFormatter(template);
    const date = new Date('2020-01-23T01:23:45.678Z');
    t.is(formatter(date), 'foo');
});

ava('string with keywords', (t) => {
    const template = 'foo{uY}/{uM}/{uD}bar{0}';
    const formatter = createDateFormatter(template);
    const date = new Date('2020-01-23T01:23:45.678Z');
    t.is(formatter(date), 'foo2020/1/23bar{0}');
});

ava('string and formatter', (t) => {
    const formatter = createDateFormatter(
        'foo{uY}/{uM}/{uD}bar{0}',
        uYYYY,
        uMMMM,
        udddd,
    );
    const date = new Date('2020-01-23T01:23:45.678Z');
    t.is(formatter(date), 'foo{uY}/{uM}/{uD}bar{0}2020JanuaryThursday');
});

ava('readme', (t) => {
    const date = new Date('2021-01-23Z12:34Z');
    const formatter1 = createDateFormatter('{lY}/{lM}/{lD} {lhh}:{lmm}:{lss}');
    t.is(formatter1(date), '2021/1/23 21:34:00');
    const formatter2 = createDateFormatter('{uY}/{uM}/{uD} {uhh}:{umm}:{uss}');
    t.is(formatter2(date), '2021/1/23 12:34:00');
});
