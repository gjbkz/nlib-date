import {testFunction} from '@nlib/test';
import {
    lZZ,
    lZ,
    lZZZ,
    lYYYY,
    lYY,
    lY,
    lMM,
    lM,
    lMMMM,
    lMMM,
    lDD,
    lD,
    ldddd,
    lddd,
    uYYYY,
    uYY,
    uY,
    uMM,
    uM,
    uMMMM,
    uMMM,
    uDD,
    uD,
    udddd,
    uddd,
    lhh,
    lh,
    lmm,
    lm,
    lss,
    ls,
    lms,
    uhh,
    uh,
    umm,
    um,
    uss,
    us,
    ums,
} from './baseFormatter';

const Date0999 = '0999-12-31T23:45:01.023Z';
const Date2020 = '2020-12-31T23:45:01.023Z';

testFunction(lZZZ, [new Date(Date2020)], '+0900');
testFunction(lZZ, [new Date(Date2020)], '+09:00');
testFunction(lZ, [new Date(Date2020)], '+9:00');

testFunction(lYYYY, [new Date(Date0999)], '1000');
testFunction(lYY, [new Date(Date0999)], '00');
testFunction(lY, [new Date(Date0999)], '1000');
testFunction(lYYYY, [new Date(Date2020)], '2021');
testFunction(lYY, [new Date(Date2020)], '21');
testFunction(lY, [new Date(Date2020)], '2021');
testFunction(lMM, [new Date(Date2020)], '01');
testFunction(lM, [new Date(Date2020)], '1');
testFunction(lMMMM, [new Date(Date2020)], 'January');
testFunction(lMMM, [new Date(Date2020)], 'Jan');
testFunction(lDD, [new Date(Date2020)], '01');
testFunction(lD, [new Date(Date2020)], '1');
testFunction(ldddd, [new Date(Date2020)], 'Friday');
testFunction(lddd, [new Date(Date2020)], 'Fri');

testFunction(uYYYY, [new Date(Date0999)], '0999');
testFunction(uYY, [new Date(Date0999)], '99');
testFunction(uY, [new Date(Date0999)], '999');
testFunction(uYYYY, [new Date(Date2020)], '2020');
testFunction(uYY, [new Date(Date2020)], '20');
testFunction(uY, [new Date(Date2020)], '2020');
testFunction(uMM, [new Date(Date2020)], '12');
testFunction(uM, [new Date(Date2020)], '12');
testFunction(uMMMM, [new Date(Date2020)], 'December');
testFunction(uMMM, [new Date(Date2020)], 'Dec');
testFunction(uDD, [new Date(Date2020)], '31');
testFunction(uD, [new Date(Date2020)], '31');
testFunction(udddd, [new Date(Date2020)], 'Thursday');
testFunction(uddd, [new Date(Date2020)], 'Thu');

testFunction(lhh, [new Date(Date2020)], '08');
testFunction(lh, [new Date(Date2020)], '8');
testFunction(lmm, [new Date(Date2020)], '45');
testFunction(lm, [new Date(Date2020)], '45');
testFunction(lss, [new Date(Date2020)], '01');
testFunction(ls, [new Date(Date2020)], '1');
testFunction(lms, [new Date(Date2020)], '023');

testFunction(uhh, [new Date(Date2020)], '23');
testFunction(uh, [new Date(Date2020)], '23');
testFunction(umm, [new Date(Date2020)], '45');
testFunction(um, [new Date(Date2020)], '45');
testFunction(uss, [new Date(Date2020)], '01');
testFunction(us, [new Date(Date2020)], '1');
testFunction(ums, [new Date(Date2020)], '023');
