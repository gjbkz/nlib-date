export interface DateFormatter {
    readonly fragments?: Array<DateFormatter | string>,
    (date: Date): string,
}

export interface TimeZoneOffset {
    sign: '-' | '+',
    hour: number,
    minute: number,
}
