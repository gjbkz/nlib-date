import {Object} from './global';
import type {DateFormatter} from './type';
import * as baseFormatters from './baseFormatter';

export const createDateFormatter = (...args: Array<DateFormatter | string>): DateFormatter => {
    const fragments = Object.freeze([...listFragments(args)]);
    return Object.defineProperty(
        (date: Date) => fragments.map((fragment) => isString(fragment) ? fragment : fragment(date)).join(''),
        'fragments',
        {value: fragments},
    );
};

const isString = (input: unknown): input is string => typeof input === 'string';

const listFragments = function* (
    fragments: Array<DateFormatter | string>,
): Generator<DateFormatter | string> {
    if (fragments.length === 1) {
        const [fragment] = fragments;
        if (isString(fragment)) {
            yield* parseTemplateString(fragment);
            return;
        }
    }
    yield* normalizeFragments(fragments);
};

const normalizeFragments = function* (fragments: Array<DateFormatter | string>): Generator<DateFormatter | string> {
    for (const fragment of fragments) {
        if (!isString(fragment) && fragment.fragments) {
            yield* normalizeFragments(fragment.fragments);
        } else {
            yield fragment;
        }
    }
};

type Formatters = Record<string, DateFormatter | undefined>;

const parseTemplateString = function* (template: string): Generator<DateFormatter | string> {
    const keywordRegExp = /\{([a-zA-Z0-9]+)\}/g;
    let matched: RegExpExecArray | null = null;
    let startIndex = keywordRegExp.lastIndex;
    // eslint-disable-next-line no-cond-assign
    while (matched = keywordRegExp.exec(template)) {
        const formatter = (baseFormatters as Formatters)[matched[1]];
        if (formatter) {
            const keywordIndex = matched.index;
            if (startIndex < keywordIndex) {
                yield template.slice(startIndex, keywordIndex);
            }
            yield formatter;
            startIndex = keywordRegExp.lastIndex;
        }
    }
    if (startIndex < template.length) {
        yield template.slice(startIndex);
    }
};
