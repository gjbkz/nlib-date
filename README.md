# @nlib/date

Utilities for Date objects.

![Test](https://github.com/nlibjs/date/workflows/Test/badge.svg)
[![codecov](https://codecov.io/gh/nlibjs/date/branch/master/graph/badge.svg)](https://codecov.io/gh/nlibjs/date)

## Usage

Assume the time zone is `+0900`.

```typescript
import {createDateFormatter} from '@nlib/date';
const date = new Date('2021-01-23Z12:34Z');
const formatter1 = createDateFormatter('{lY}/{lM}/{lD} {lhh}:{lmm}:{lss}');
console.debug(formatter1(date)) // → '2021/1/23 21:34:00'
const formatter2 = createDateFormatter('{uY}/{uM}/{uD} {uhh}:{umm}:{uss}');
console.debug(formatter2(date)) // → '2021/1/23 12:34:00'
```

## List of keywords

[See the test code.](src/baseFormatter.test.ts)
